# mf-tf-users

## Stategy:

* Split code into modules and definitions
* Create a user module that creates environment users based on boolean in definition
* Use Hashicorp's AWS modules (https://github.com/terraform-aws-modules/terraform-aws-iam/tree/master/modules/iam-user)
* Create a users definition with an entry for each user

### Pros:
 
* Can ensure all users in each envionment get same roles and policies assigned (not implemented)
* Users can be enabled/disabled on an environemnt basis by setting the environment boolean to  true/false.
* Easy to see what environments a person has access to

### Cons:

* Outputs for each user is complex
* Still not able to iterate over list to create modules :(, so definition is overly long. Preference would be to define users in maps not code.

### Usage

* Set your AWS secret and access keys
* cd $REPO/definitions 
* terraform init && terraform apply

