# Definition manifest for users
module "mf_user_nevsa" {

  source     = "../modules/mf-users/"
  users_name = "nevsa"
  dev        = "true"
  qa         = "true"
  uat        = "true"
  test       = "true"
  prod       = "true"

}
module "mf_user_cordelia" {

  source     = "../modules/mf-users/"
  users_name = "cordelia"
  dev        = "true"
  qa         = "true"
  uat        = "true"
  test       = "true"
  prod       = "true"

}
module "mf_user_kriste" {

  source     = "../modules/mf-users/"
  users_name = "kriste"
  dev        = "true"
  qa         = "true"
  uat        = "true"
  test       = "true"
  prod       = "true"

}
module "mf_user_darleen" {

  source     = "../modules/mf-users/"
  users_name = "darleen"
  dev        = "true"
  qa         = "true"
  uat        = "true"
  test       = "true"
  prod       = "true"

}

module "mf_user_wynnie" {

  source     = "../modules/mf-users/"
  users_name = "wynnie"
  dev        = "true"
  qa         = "true"
  uat        = "true"
  test       = "true"
  prod       = "true"

}

module "mf_user_vonnie" {

  source     = "../modules/mf-users/"
  users_name = "vonnie"
  dev        = "true"
  qa         = "true"
  uat        = "true"
  test       = "true"
  prod       = "true"

}

module "mf_user_emelita" {

  source     = "../modules/mf-users/"
  users_name = "emelita"
  dev        = "true"
  qa         = "true"
  uat        = "true"
  test       = "true"
  prod       = "true"

}

module "mf_user_maurita" {

  source     = "../modules/mf-users/"
  users_name = "maurita"
  dev        = "true"
  qa         = "true"
  uat        = "true"
  test       = "true"
  prod       = "true"

}

module "mf_user_devinne" {

  source     = "../modules/mf-users/"
  users_name = "devinne"
  dev        = "true"
  qa         = "true"
  uat        = "true"
  test       = "true"
  prod       = "true"

}


module "mf_user_breena" {

  source     = "../modules/mf-users/"
  users_name = "breena"
  dev        = "true"
  qa         = "true"
  uat        = "true"
  test       = "true"
  prod       = "true"

}
