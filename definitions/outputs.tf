output "User_nevsa_access_info" {
  value = join("\n", ["\n", module.mf_user_nevsa.this_mf_iam_user_dev_access_info, module.mf_user_nevsa.this_mf_iam_user_qa_access_info, module.mf_user_nevsa.this_mf_iam_user_uat_access_info, module.mf_user_nevsa.this_mf_iam_user_test_access_info, module.mf_user_nevsa.this_mf_iam_user_prod_access_info, "\n"])
}

output "User_cordelia_access_info" {
  value = join("\n", ["\n", module.mf_user_cordelia.this_mf_iam_user_dev_access_info, module.mf_user_cordelia.this_mf_iam_user_qa_access_info, module.mf_user_cordelia.this_mf_iam_user_uat_access_info, module.mf_user_cordelia.this_mf_iam_user_test_access_info, module.mf_user_cordelia.this_mf_iam_user_prod_access_info, "\n"])
}

output "User_kriste_access_info" {
  value = join("\n", ["\n", module.mf_user_nevsa.this_mf_iam_user_dev_access_info, module.mf_user_nevsa.this_mf_iam_user_qa_access_info, module.mf_user_nevsa.this_mf_iam_user_uat_access_info, module.mf_user_nevsa.this_mf_iam_user_test_access_info, module.mf_user_nevsa.this_mf_iam_user_prod_access_info, "\n"])
}

output "User_darleen_access_info" {
  value = join("\n", ["\n", module.mf_user_darleen.this_mf_iam_user_dev_access_info, module.mf_user_darleen.this_mf_iam_user_qa_access_info, module.mf_user_darleen.this_mf_iam_user_uat_access_info, module.mf_user_darleen.this_mf_iam_user_test_access_info, module.mf_user_darleen.this_mf_iam_user_prod_access_info, "\n"])
}

output "User_wynnie_access_info" {
  value = join("\n", ["\n", module.mf_user_wynnie.this_mf_iam_user_dev_access_info, module.mf_user_wynnie.this_mf_iam_user_qa_access_info, module.mf_user_wynnie.this_mf_iam_user_uat_access_info, module.mf_user_wynnie.this_mf_iam_user_test_access_info, module.mf_user_wynnie.this_mf_iam_user_prod_access_info, "\n"])
}

output "User_vonnie_access_info" {
  value = join("\n", ["\n", module.mf_user_vonnie.this_mf_iam_user_dev_access_info, module.mf_user_vonnie.this_mf_iam_user_qa_access_info, module.mf_user_vonnie.this_mf_iam_user_uat_access_info, module.mf_user_vonnie.this_mf_iam_user_test_access_info, module.mf_user_vonnie.this_mf_iam_user_prod_access_info, "\n"])
}

output "User_emelita_access_info" {
  value = join("\n", ["\n", module.mf_user_emelita.this_mf_iam_user_dev_access_info, module.mf_user_emelita.this_mf_iam_user_qa_access_info, module.mf_user_emelita.this_mf_iam_user_uat_access_info, module.mf_user_emelita.this_mf_iam_user_test_access_info, module.mf_user_emelita.this_mf_iam_user_prod_access_info, "\n"])
}

output "User_maurita_access_info" {
  value = join("\n", ["\n", module.mf_user_maurita.this_mf_iam_user_dev_access_info, module.mf_user_maurita.this_mf_iam_user_qa_access_info, module.mf_user_maurita.this_mf_iam_user_uat_access_info, module.mf_user_maurita.this_mf_iam_user_test_access_info, module.mf_user_maurita.this_mf_iam_user_prod_access_info, "\n"])
}

output "User_devinne_access_info" {
  value = join("\n", ["\n", module.mf_user_devinne.this_mf_iam_user_dev_access_info, module.mf_user_devinne.this_mf_iam_user_qa_access_info, module.mf_user_devinne.this_mf_iam_user_uat_access_info, module.mf_user_devinne.this_mf_iam_user_test_access_info, module.mf_user_devinne.this_mf_iam_user_prod_access_info, "\n"])
}

output "User_breena_access_info" {
  value = join("\n", ["\n", module.mf_user_breena.this_mf_iam_user_dev_access_info, module.mf_user_breena.this_mf_iam_user_qa_access_info, module.mf_user_breena.this_mf_iam_user_uat_access_info, module.mf_user_breena.this_mf_iam_user_test_access_info, module.mf_user_breena.this_mf_iam_user_prod_access_info, "\n"])
}
