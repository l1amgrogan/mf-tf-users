variable users_name {
  description = "User's real name"
  default     = ""
}
variable dev {
  description = "Boolean to decide if _dev account shoult be created"
  default     = "false"
}
variable qa {
  description = "Boolean to decide if _qa account shoult be created"
  default     = "false"
}
variable uat {
  description = "Boolean to decide if _uat account shoult be created"
  default     = "false"
}
variable test {
  description = "Boolean to decide if _test account shoult be created"
  default     = "false"
}
variable prod {
  description = "Boolean to decide if _prod account shoult be created"
  default     = "false"
}
