
output "this_mf_iam_user_dev_access_info" {
  description = "Printing access info"
  value       = join(":", [module.mf_iam_user_dev.this_iam_user_name, module.mf_iam_user_dev.this_iam_access_key_id, module.mf_iam_user_dev.this_iam_access_key_secret])
}

output "this_mf_iam_user_qa_access_info" {
  description = "Printing access info"
  value       = join(":", [module.mf_iam_user_qa.this_iam_user_name, module.mf_iam_user_qa.this_iam_access_key_id, module.mf_iam_user_qa.this_iam_access_key_secret])
}

output "this_mf_iam_user_uat_access_info" {
  description = "Printing access info"
  value       = join(":", [module.mf_iam_user_uat.this_iam_user_name, module.mf_iam_user_uat.this_iam_access_key_id, module.mf_iam_user_uat.this_iam_access_key_secret])
}

output "this_mf_iam_user_test_access_info" {
  description = "Printing access info"
  value       = join(":", [module.mf_iam_user_test.this_iam_user_name, module.mf_iam_user_test.this_iam_access_key_id, module.mf_iam_user_test.this_iam_access_key_secret])
}

output "this_mf_iam_user_prod_access_info" {
  description = "Printing access info"
  value       = join(":", [module.mf_iam_user_prod.this_iam_user_name, module.mf_iam_user_prod.this_iam_access_key_id, module.mf_iam_user_prod.this_iam_access_key_secret])
}

