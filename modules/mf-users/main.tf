#######################################################################################
#  Users Module
#
#
# Wrapper for https://github.com/terraform-aws-modules/terraform-aws-iam/tree/master/modules/iam-user
#
# In this module we create:
#   * dev, qa, uat, test and prod user accounts
#
# Usage:
#  When calling this module you need to set boolean variables for each account type, e.g. if you want to create a uat account set "uat = true" in your definition
#
# Exampe:
# 
# module "mf_user_emelita" {
#
#  source = "../modules/mf-users/"
#  users_name   = "emelita"
#  dev    = "true"
#  qa     = "true"
#  uat    = "true"
#  test   = "true"
#  prod   = "true"
#
# }
#
#
# Further thinking: 
#   By creating the users in groups like this it will make it easy to add roles or 
#   policy documents relating to those environments to each user
#     
#######################################################################################

module "mf_iam_user_dev" {

  source  = "terraform-aws-modules/iam/aws//modules/iam-user"
  version = "2.3.0"

  name                          = join("-", [var.users_name, "dev"])
  force_destroy                 = true
  create_user                   = var.dev
  create_iam_user_login_profile = false
  create_iam_access_key         = true
}

module "mf_iam_user_qa" {

  source  = "terraform-aws-modules/iam/aws//modules/iam-user"
  version = "2.3.0"

  name                          = join("-", [var.users_name, "qa"])
  force_destroy                 = true
  create_user                   = var.qa
  create_iam_user_login_profile = false
  create_iam_access_key         = true
}

module "mf_iam_user_uat" {

  source  = "terraform-aws-modules/iam/aws//modules/iam-user"
  version = "2.3.0"

  name                          = join("-", [var.users_name, "uat"])
  force_destroy                 = true
  create_user                   = var.uat
  create_iam_user_login_profile = false
  create_iam_access_key         = true
}

module "mf_iam_user_test" {

  source  = "terraform-aws-modules/iam/aws//modules/iam-user"
  version = "2.3.0"

  name                          = join("-", [var.users_name, "test"])
  force_destroy                 = true
  create_user                   = var.test
  create_iam_user_login_profile = false
  create_iam_access_key         = true
}

module "mf_iam_user_prod" {

  source  = "terraform-aws-modules/iam/aws//modules/iam-user"
  version = "2.3.0"

  name                          = join("-", [var.users_name, "prod"])
  force_destroy                 = true
  create_user                   = var.prod
  create_iam_user_login_profile = false
  create_iam_access_key         = true
}
