
variable users_name {
  description = "User's real name"
}

variable dev {
  description = "Boolean to decide if _dev account should be created"
  default     = "false"
}

variable qa {
  description = "Boolean to decide if _qa account should be created"
  default     = "false"
}

variable uat {
  description = "Boolean to decide if _uat account should be created"
  default     = "false"
}

variable test {
  description = "Boolean to decide if _test account should be created"
  default     = "false"
}

variable prod {
  description = "Boolean to decide if _prod account should be created"
  default     = "false"
}
